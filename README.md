# nu_plugin_ron

This is a [nushell] [plugin] to parse [RON] text into `nu` structured types.

[nushell]: https://www.nushell.sh/
[plugin]: https://www.nushell.sh/contributor-book/plugins.html
[RON]: https://github.com/ron-rs/ron

# Installing

[add the plugin]: https://www.nushell.sh/book/plugins.html#adding-a-plugin
[`register`]: https://www.nushell.sh/book/commands/register.html

To [add the plugin] permanently, just install it and call [`register`] on it:

## Using Cargo

```bash
cargo install nu_plugin_ron
register ~/.cargo/bin/nu_plugin_ron
```

## Usage

Consider the following RON example:
```rust
Scene( // class name is optional
    materials: { // this is a map
        "metal": (
            reflectivity: 1.0,
        ),
        "plastic": (
            reflectivity: 0.5,
        ),
    },
    entities: [ // this is an array
        (
            name: "hero",
            material: "metal",
        ),
        (
            name: "monster",
            material: "plastic",
        ),
    ],
)
```

```bash
$ open example.ron
╭───────────┬───────────────────────────────────────╮
│           │ ╭───┬──────────┬─────────╮            │
│ entities  │ │ # │ material │  name   │            │
│           │ ├───┼──────────┼─────────┤            │
│           │ │ 0 │ metal    │ hero    │            │
│           │ │ 1 │ plastic  │ monster │            │
│           │ ╰───┴──────────┴─────────╯            │
│           │ ╭─────────┬─────────────────────────╮ │
│ materials │ │         │ ╭──────────────┬──────╮ │ │
│           │ │ metal   │ │ reflectivity │ 1.00 │ │ │
│           │ │         │ ╰──────────────┴──────╯ │ │
│           │ │         │ ╭──────────────┬──────╮ │ │
│           │ │ plastic │ │ reflectivity │ 0.50 │ │ │
│           │ │         │ ╰──────────────┴──────╯ │ │
│           │ ╰─────────┴─────────────────────────╯ │
╰───────────┴───────────────────────────────────────╯

$ open example.ron | update materials.metal.reflectivity 2.00 | save -f new_example.ron
$ open new_example.ron
╭───────────┬───────────────────────────────────────╮
│           │ ╭───┬──────────┬─────────╮            │
│ entities  │ │ # │ material │  name   │            │
│           │ ├───┼──────────┼─────────┤            │
│           │ │ 0 │ metal    │ hero    │            │
│           │ │ 1 │ plastic  │ monster │            │
│           │ ╰───┴──────────┴─────────╯            │
│           │ ╭─────────┬─────────────────────────╮ │
│ materials │ │         │ ╭──────────────┬──────╮ │ │
│           │ │ metal   │ │ reflectivity │ 2.00 │ │ │
│           │ │         │ ╰──────────────┴──────╯ │ │
│           │ │         │ ╭──────────────┬──────╮ │ │
│           │ │ plastic │ │ reflectivity │ 0.50 │ │ │
│           │ │         │ ╰──────────────┴──────╯ │ │
│           │ ╰─────────┴─────────────────────────╯ │
╰───────────┴───────────────────────────────────────╯
```
