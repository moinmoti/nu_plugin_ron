use nu_plugin::{serve_plugin, MsgPackSerializer};
use nu_plugin_ron::NuRon;

fn main() {
    serve_plugin(&mut NuRon {}, MsgPackSerializer {})
}
