pub mod transform;

use nu_plugin::{EvaluatedCall, LabeledError, Plugin};
use nu_protocol::{Category, PluginSignature, ShellError, Type, Value as NuValue};
use ron::{ser::PrettyConfig, Value as RonValue};

pub struct NuRon;

impl Plugin for NuRon {
    fn signature(&self) -> Vec<PluginSignature> {
        vec![
            PluginSignature::build("from ron")
                .input_output_type(Type::String, Type::Record(vec![]))
                .usage("Transform RON text to nushell table")
                .category(Category::Formats),
            PluginSignature::build("to ron")
                .input_output_type(Type::Record(vec![]), Type::String)
                .usage("Transform nushell table to RON text")
                .category(Category::Formats),
        ]
    }

    fn run(
        &mut self,
        name: &str,
        call: &EvaluatedCall,
        input: &NuValue,
    ) -> Result<NuValue, LabeledError> {
        let span = call.head;
        match name {
            "from ron" => {
                let ron_val: RonValue =
                    match input.as_string().expect("input is not a string").parse() {
                        Ok(val) => val,
                        Err(err) => {
                            return Err(LabeledError {
                                label: err.code.to_string(),
                                msg: format!(
                                    "Error encountered on line: {}, col: {} while parsing the doc",
                                    err.position.line, err.position.col
                                ),
                                span: Some(span),
                            })
                        }
                    };
                match transform::ron_to_nu(ron_val, span) {
                    Ok(val) => Ok(val),
                    Err(err) => Err(LabeledError {
                        label: err.to_string(),
                        msg: "Unable to transform Nushell table to RON text".into(),
                        span: Some(span),
                    }),
                }
            }
            "to ron" => {
                let ron_val = transform::nu_to_ron(input, span).unwrap();
                Ok(NuValue::string(
                    ron::ser::to_string_pretty(&ron_val, PrettyConfig::new()).unwrap(),
                    span,
                ))
            }
            _ => Err(LabeledError {
                label: ShellError::DidYouMean("from ron / to ron".into(), span).to_string(),
                msg: "RON plugin for Nushell only supports from/to".into(),
                span: Some(span),
            }),
        }
    }
}
