use nu_protocol::{ast::PathMember, ShellError, Span, Value as NuValue};
use ron::{Number, Value as RonValue};

pub fn ron_to_nu(ron_val: RonValue, span: Span) -> Result<NuValue, ShellError> {
    let nu_val = match ron_val {
        RonValue::Bool(val) => NuValue::bool(val, span),
        RonValue::Char(char) => NuValue::string(char, span),
        RonValue::Map(map) => NuValue::record(
            map.into_iter()
                .map(|(k, v)| {
                    (
                        ron_to_nu(k, span).unwrap().as_string().unwrap(),
                        ron_to_nu(v, span).unwrap(),
                    )
                })
                .collect(),
            span,
        ),
        RonValue::Number(num) => match num {
            ron::Number::Integer(i) => NuValue::int(i, span),
            ron::Number::Float(f) => NuValue::float(f.get(), span),
        },
        RonValue::Option(opt) => match opt {
            Some(boxed_val) => ron_to_nu(*boxed_val, span)?,
            None => NuValue::nothing(span),
        },
        RonValue::String(val) => NuValue::string(val, span),
        RonValue::Seq(vals) => NuValue::list(
            vals.into_iter()
                .map(|v| ron_to_nu(v, span).unwrap())
                .collect(),
            span,
        ),
        RonValue::Unit => NuValue::nothing(span),
    };
    Ok(nu_val)
}

pub fn nu_to_ron(nu_val: &NuValue, span: Span) -> Result<RonValue, ShellError> {
    fn ron_num(v: impl Into<Number>) -> RonValue {
        RonValue::Number(ron::Number::new(v))
    }

    let ron_val = match nu_val {
        NuValue::Bool { val, .. } => RonValue::Bool(*val),
        NuValue::Int { val, .. } => ron_num(*val),
        NuValue::Float { val, .. } => ron_num(*val),
        NuValue::Filesize { val, .. } => ron_num(*val),
        NuValue::Duration { val, .. } => ron_num(*val),
        NuValue::Date { val, .. } => RonValue::String(val.to_string()),
        NuValue::String { val, .. } => RonValue::String(val.clone()),
        NuValue::Record { val, .. } => RonValue::Map(
            val.into_iter()
                .map(|v| (RonValue::String(v.0.clone()), nu_to_ron(v.1, span).unwrap()))
                .collect(),
        ),
        NuValue::List { vals, .. } => RonValue::Seq(
            vals.into_iter()
                .map(|v| nu_to_ron(v, span).unwrap())
                .collect(),
        ),
        NuValue::Binary { val, .. } => {
            RonValue::Seq(val.into_iter().map(|x| ron_num(*x as u64)).collect())
        }
        NuValue::CellPath { val, .. } => RonValue::Seq(
            val.members
                .iter()
                .map(|x| match &x {
                    PathMember::String { val, .. } => RonValue::String(val.clone()),
                    PathMember::Int { val, .. } => ron_num(*val as u64),
                })
                .collect(),
        ),
        NuValue::CustomValue { val, .. } => {
            let collected = val.to_base_value(span)?;
            nu_to_ron(&collected, span)?
        }
        NuValue::LazyRecord { val, .. } => {
            let collected = val.collect()?;
            nu_to_ron(&collected, span)?
        }
        NuValue::Block { .. }
        | NuValue::Closure { .. }
        | NuValue::MatchPattern { .. }
        | NuValue::Range { .. }
        | NuValue::Nothing { .. } => RonValue::Unit,
        NuValue::Error { error, .. } => return Err(*error.clone()),
    };
    Ok(ron_val)
}
